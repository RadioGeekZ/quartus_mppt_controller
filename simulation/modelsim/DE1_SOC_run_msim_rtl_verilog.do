transcript on
if ![file isdirectory DE1_SOC_iputf_libs] {
	file mkdir DE1_SOC_iputf_libs
}

if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

###### Libraries for IPUTF cores 
###### End libraries for IPUTF cores 
###### MIF file copy and HDL compilation commands for IPUTF cores 


vlog "D:/Pr_Quartus/quartus_mppt_controller/pll_5mhz_sim/pll_5mhz.vo"

vlog -vlog01compat -work work +incdir+D:/Pr_Quartus/quartus_mppt_controller {D:/Pr_Quartus/quartus_mppt_controller/pll_5mhz.vo}
vlog -vlog01compat -work work +incdir+D:/Pr_Quartus/quartus_mppt_controller {D:/Pr_Quartus/quartus_mppt_controller/measure_uip.v}
vlog -vlog01compat -work work +incdir+D:/Pr_Quartus/quartus_mppt_controller {D:/Pr_Quartus/quartus_mppt_controller/counter.v}
vlog -vlog01compat -work work +incdir+D:/Pr_Quartus/quartus_mppt_controller {D:/Pr_Quartus/quartus_mppt_controller/pwm_control.v}
vlog -vlog01compat -work work +incdir+D:/Pr_Quartus/quartus_mppt_controller {D:/Pr_Quartus/quartus_mppt_controller/counter_50M.v}
vlog -vlog01compat -work work +incdir+D:/Pr_Quartus/quartus_mppt_controller {D:/Pr_Quartus/quartus_mppt_controller/meas_st_gen.v}
vlog -vlog01compat -work work +incdir+D:/Pr_Quartus/quartus_mppt_controller {D:/Pr_Quartus/quartus_mppt_controller/de1_soc.v}
vlib pll_5mhz
vmap pll_5mhz pll_5mhz
vlog -vlog01compat -work pll_5mhz +incdir+D:/Pr_Quartus/quartus_mppt_controller/pll_5mhz {D:/Pr_Quartus/quartus_mppt_controller/pll_5mhz/pll_5mhz_0002.v}


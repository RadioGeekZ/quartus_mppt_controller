module mult_8bit (
	input unsigned [7:0] A,
	input unsigned [7:0] B,
	output unsigned [15:0] result
);

	assign result = A * B;
	
endmodule

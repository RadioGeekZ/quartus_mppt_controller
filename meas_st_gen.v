module meas_st_gen (
	input clk,
	output [0:0] MEAS_ST
);

wire [25:0] counter_out;
reg [0:0] meas_st;

assign MEAS_ST = meas_st;

counter_50M cnt (
	.clk(clk),
	.counter_out(counter_out)
);

always @ (negedge clk)
begin
	if (counter_out == 26'd0)
		meas_st <= 1'b1;
	else if (counter_out == 26'd1)
		meas_st <= 1'b0;
	else 
		meas_st <= 1'b0;
	
end


endmodule

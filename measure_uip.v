module measure_uip (
	input [0:0] MEAS_ST,
	input [7:0] u,
	input [7:0] i,
	inout reg [7:0] u_old,
	inout reg [7:0] i_old,
	inout reg [7:0] p_old,
	inout reg [7:0] pwm_cw
);

reg signed [7:0] delta_p;
reg signed [7:0] delta_v;

reg [7:0] u_new;
reg [7:0] i_new;
reg [7:0] p_new;

always @(posedge MEAS_ST)
begin
	u_new = u;
	i_new = i;
	p_new = u * i;
end

always @(negedge MEAS_ST)
begin
	delta_p = p_new - p_old;
	delta_v = u_new - u_old;
	
	if (delta_p < 0)
		if (delta_v < 0)
			pwm_cw = pwm_cw + 1;
		else
			pwm_cw = pwm_cw -1;
	else if (delta_p > 0)
		if (delta_v < 0)
			pwm_cw = pwm_cw + 1;
		else
			pwm_cw = pwm_cw -1;
	
	p_old = p_new;
	u_old = u_new;
	i_old = i_new;
end

endmodule

module DE1_SOC(
	CLOCK_50, KEY, SW, LEDR, ADC_SCLK,
	ADC_CONVST, ADC_DOUT, ADC_DIN
);


input CLOCK_50;
input [0:0] KEY;
input [2:0] SW;
output [9:0] LEDR;

input ADC_DOUT;
output ADC_SCLK, ADC_CONVST, ADC_DIN;
//=======================================================
//  REG/WIRE declarations
//=======================================================



wire [11:0] pv_u; //Напряжение считанное с фотоэлемента
wire [11:0] pv_i; //Напряжение пропорциональное измеренному току фотоэлемента

wire [7:0] u_old;
wire [7:0] i_old;
wire [7:0] p_old;

wire [0:0] MEAS_ST;


wire [11:0] values [5:0]; //Остальные каналы АЦП
wire [0:0] clk_5mhz;

//=======================================================
//  Structural coding
//=======================================================
assign LEDR [7:0] = pv_u [11:4];


adc_control ADC (
	.CLOCK (CLOCK_50),
	.RESET (!KEY[0]),
	.ADC_SCLK (ADC_SCLK),
	.ADC_CS_N (ADC_CONVST),
	.ADC_DOUT (ADC_DOUT),
	.ADC_DIN (ADC_DIN),
	.CH0 (pv_u),
	.CH1 (pv_i),
	.CH2 (values[0]),
	.CH3 (values[1]),
	.CH4 (values[2]),
	.CH5 (values[3]),
	.CH6 (values[4]),
	.CH7 (values[5])
);

//генератор 5 МГЦ меандра для ШИМ
pll_5mhz pll_5mhz_unit (
		.refclk(CLOCK_50),   //  refclk.clk
		.rst(1'b0),      //   reset.reset
		.outclk_0(clk_5mhz)  // outclk0.clk
	);

//Контроллер ШИМ
pwm_controller pwm_inst (
	.PWM_CW(pv_u[11:4]),
	.clk(clk_5mhz),
	.PWM_OUT(LEDR[9])
);


//Производит измерение напряжения, тока и мощности, 
//записывает данные в память. Выполняет алгоритм P&O
//Возвращает скважность для контроллера ШИМ
measure_uip measure_uip_inst (
	.MEAS_ST(MEAS_ST),
	.u(pv_u[11:4]),
	.i(pv_i[11:4]),
	.u_old(u_old),
	.i_old(i_old),
	.p_old(p_old),
	.pwm_cw(PWM_CW)
);

//Генерирует импульс для записи данных в память
meas_st_gen meas_st_gen_inst (
	.clk(clk_5mhz),
	.MEAS_ST(MEAS_ST)
);

endmodule

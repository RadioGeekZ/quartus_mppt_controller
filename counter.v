`timescale 1ps / 1ps
module counter (
	clk,
	counter_out
);

input clk;
output reg [7:0] counter_out;

always @(posedge clk)
begin
	counter_out <= #1 counter_out + 1'b1;
end

endmodule

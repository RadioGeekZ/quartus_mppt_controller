`timescale 1ps/1ps
module meas_st_gen_tb;

reg clk = 1'b0;
wire meas_st;
wire cnt_out;

meas_st_gen uut (
	.clk(clk),
	.MEAS_ST(meas_st)
);

counter_50M cnt (
	.clk(clk),
	.counter_out(cnt_out)
);

initial
begin
	while (1'b1)
	begin
		clk = ~clk;
		#1;
	end
end

endmodule

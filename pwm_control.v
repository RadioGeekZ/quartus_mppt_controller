module pwm_controller (
	PWM_CW,
	PWM_OUT,
	clk,
);

input clk;            //Port type declared
input [7:0] PWM_CW;   // 8 bit PWM input

output reg PWM_OUT; // 1 bit PWM output
wire [7:0] counter_out;  // 8 bit counter output



always @ (posedge clk)
begin
		
	if (PWM_CW > counter_out)
		PWM_OUT <= 1;
		
	else 
		PWM_OUT <= 0;
end
	

counter counter_inst(
	.clk (clk),
	.counter_out (counter_out)
);
	
endmodule

